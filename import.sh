#!/bin/sh -e

BASE_DIR="$(pwd)"

for pkg in \
 ctfutils \
 cuse4bsd \
 freebsd-buildutils \
 freebsd-glue \
 freebsd-libs \
 freebsd-manpages \
 freebsd-quota \
 freebsd-smbfs \
 freebsd-utils \
 fuse4bsd \
 istgt \
 kfreebsd-10 \
 kfreebsd-11 \
 kfreebsd-defaults \
 kfreebsd-downloader-10 \
 kfreebsd-kernel-headers \
 ufsutils \
 xserver-xorg-video-nv \
 zfsutils \

do
	mkdir "$BASE_DIR/$pkg"
	cd "$BASE_DIR/$pkg"
	git svn init \
	 --trunk="trunk/$pkg/" \
	 --branches="branches/squeeze/$pkg" \
	 --branches="branches/wheezy/$pkg" \
	 --branches="branches/jessie/$pkg" \
	 --branches="branches/experimental/$pkg" \
	 svn://anonscm.debian.org/glibc-bsd/
	git svn fetch \
	 --authors-file="$BASE_DIR/authors.txt"
	git filter-branch --commit-filter 'git_commit_non_empty_tree "$@"' HEAD
done
